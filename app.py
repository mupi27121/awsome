from datetime import date

from flask import Flask, render_template, request, url_for
import mysql.connector
# import errorcode
from mysql.connector import errorcode
from flask import Flask, redirect

app = Flask(__name__)


@app.route('/sus')
def susload():
    print(request.args)
    if request.args.get("search") is not None:
        sqltext = "Noch nichts vorhanden"
        sqldate = "08.07.2002"
        sqlinfo = ""
        sqlclass = ""
        sqlsubject = request.args.get("search")
        global cnx, search
        search = request.args.get("search")
        # get db connection
        print(search)
        try:
            search = request.args.get("search")
            cnx = mysql.connector.connect(
                host="localhost",
                user="sus",
                passwd="password1",
                database="notes",
                port=3306
            )

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print('Invalid credential. Unable to access database.')
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print('Database does not exists')
            else:
                print('Failed to connect to database')
        # now fetch data
        try:
            # cursor
            cnx_cursor = cnx.cursor()
            # TODO String single line

            # sql query
            query1 = "SELECT * FROM notizen WHERE Fach "
            query2 = "= "
            query3 = '\"'
            query4 = "\" ORDER BY Datum DESC LIMIT 1"
            query = query1 + query2 + query3 + search + query4
            # execute
            cnx_cursor.execute(query)

            # result
            result = cnx_cursor.fetchall()

            # print result
            print('Total rows: %d' % cnx_cursor.rowcount)
            for row in result:
                sqltext = row[4]
                sqldate = row[0]
                sqlinfo = row[3]
                sqlclass = row[1]
                sqlsubject = row[2]
        except mysql.connector.Error as err:
            print("Error:", err.msg)
            # close connection
            cnx.close()
        except:
            print("Unknown error occurred!")
            # close connection
            cnx.close()
        finally:
            # close cursor
            cnx_cursor.close()
            # close connection
            cnx.close()
        return render_template("sus.html", date=sqldate, info=sqlinfo, text=sqltext, klasse=sqlclass, subject=sqlsubject)
        pass
    else:
        return render_template("sus.html")


@app.route('/teacher')
def hello_world():
    return render_template('teacher.html')


@app.route('/teacher', methods=['POST'])
def result():
    if request.method == 'POST':
        text = request.form['text']
        theme = request.form['thema']
        klasse = request.form['class']
        fach = request.form['fach']
        print(theme)
        print(text)
        print(klasse)
        print(fach)
        mysqlwrite(klasse, fach, theme, text)
        return render_template("teacher.html")


def mysqlwrite(klasse, fach, theme, text):
    mydb = mysql.connector.connect(
        host="localhost",
        user="teacher",
        passwd="password",
        database="notes",
        port=3306
    )

    mycursor = mydb.cursor()

    sql = "INSERT INTO notizen (Datum, Klasse, Fach, Betreff, Text) VALUES (%s, %s, %s, %s, %s)"
    val = (date.today(), klasse, fach, theme, text)
    mycursor.execute(sql, val)

    mydb.commit()

    print(mycursor.rowcount, "record inserted.")
    pass


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
